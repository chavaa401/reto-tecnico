// ****** Variables **********
const btnSearchInvoices = document.querySelector('#search-invoice');
const clientKey = document.querySelector('#client-key');
const clientCurrency = document.querySelector('#rates-select');
const formPayInvoiceBalance = document.querySelector('#pay-invoice-balance');
const formSearchInvoice = document.querySelector('#form-invoice');
const inputDateDocument = document.querySelector("#date-document");
const invoiceSelectPay = document.querySelector('#invoice-select-pay');
const selectRates = document.querySelector('#rates-select');
const selectChangeCurrency = document.querySelector('#rates-select');
const amountPayable = document.querySelector('#amount-payable');

let currencyGlobal = 'MXN';
let currencyGlobalValue = 1;
let exchangesRates = false;
let invoicesToPay = false;


// ****** Variables **********

// ****** Events **********

formSearchInvoice.addEventListener("submit", getInvoices);
formPayInvoiceBalance.addEventListener("submit", payInvoiceBalance);
selectChangeCurrency.addEventListener("change", changeCurrency);

function dateDocumentMaxToday() {
    let today = new Date().toJSON().split('T')[0];
    inputDateDocument.setAttribute("max", today);
}

function getRates() {
    fetch('https://api.exchangeratesapi.io/latest?base=MXN')
        .then(function (resp) {
            return resp.json();
        })
        .then(function (resp) {
            exchangesRates = resp;
        })
        .catch(function () {
            console.error('Error when obtaining exchange rates');
            exchangesRates = false;
        });
}

function addRates() {
    let option;
    option = document.createElement("option");
    option.text = 'MXN - 1';
    option.value = 1;
    option.dataset.currency = currencyGlobal;
    selectRates.appendChild(option);
    if (exchangesRates) {
        let rates = exchangesRates.rates;
        for (let index in rates) {
            if (index != currencyGlobal) {
                option = document.createElement("option");
                option.text = index + ' - ' + Math.round(rates[index] * 100) / 100;
                option.value = Math.round(rates[index] * 100) / 100;
                option.dataset.currency = index;
                selectRates.appendChild(option);
            }
        }
    }
}

function getInvoices(e) {
    e.preventDefault();
    invoicesToPay = JSON.parse(localStorage.getItem('invoices'));
    if (invoicesToPay === null) {
        invoicesToPay = [];
    } else {
        invoicesToPay.forEach(function (itemInvoice, index) {
            itemInvoice.items.forEach(function (itemInvoiceMov, itm) {
                invoicesToPay[index]['totalToPay'] += parseFloat(itemInvoiceMov.movSubtotal);
            });
            if (itemInvoice.clientKey == clientKey.value && itemInvoice.currency == currencyGlobal) {
                listInvoices(invoicesToPay[index]);
            }
        });
        addToLocalStorage('invoices', invoicesToPay);
    }
}

function listInvoices(item) {
    let option;
    option = document.createElement("option");
    option.text = item.dateDocument + ' - ' + '$' + item.totalToPay.toFixed(2) + ' - ' + item.currency;
    option.value = item.totalToPay.toFixed(2);
    option.dataset.currency = item.currency;
    option.dataset.currencyVal = item.currencyValue;
    invoiceSelectPay.appendChild(option);
}

function changeCurrency(e) {
    e.preventDefault();
    currencyGlobal = selectChangeCurrency.options[selectChangeCurrency.selectedIndex].getAttribute("data-currency");
    currencyGlobalValue = selectChangeCurrency.value;
}

function payInvoiceBalance(e) {
    e.preventDefault();
    invoicesToPay = JSON.parse(localStorage.getItem('invoices'));
    invoicesToPay.forEach(function (itemInvoice, index) {
        if (itemInvoice.clientKey == clientKey.value && itemInvoice.totalToPay == invoiceSelectPay.value) {
            if (amountPayable.value > itemInvoice.totalToPay) {
                alert('Factura Pagada');
                invoicesToPay[index]['pay'] = true;
                addToLocalStorage('invoices', invoicesToPay);
                invoiceSelectPay.innerHTML = '';
                formSearchInvoice.reset();
                formPayInvoiceBalance.reset();
            } else {
                alert('Fondos Insuficientes');
            }
        }
    });
}

// ****** local storage **********
function addToLocalStorage(key, values) {
    localStorage.setItem(key, JSON.stringify(values));
}
// ****** local storage **********

document.addEventListener('DOMContentLoaded', function () {
    dateDocumentMaxToday();
    getRates();
    setTimeout(() => {
        addRates();
    }, 1500);
});