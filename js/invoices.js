// ****** Variables **********
const btnEndInvoice = document.querySelector('#form-invoice');
const btnChangeCurrency = document.querySelector('.change-currency');
const formInvoice = document.querySelector('#form-invoice');
const formInvoiceItems = document.querySelector('#form-invoice-items');
const inputDateDocument = document.querySelector("#date-document");
const inputGlobalDiscount = document.querySelector("#global-discount");
const selectRates = document.querySelector('#rates-select');
const tableInvoiceItems = document.querySelector('#table-invoice-items');
const tableInvoiceItemsPrint = document.querySelector('#table-invoice-items-print');

let exchangesRates = false;
let arrayInvoiceItems = [];
let arrayInvoices = [];
let currencyGlobal = 'MXN';
let currencyGlobalValue = 1;
// ****** Variables **********

// ****** Events **********
btnEndInvoice.addEventListener("submit", createInvoice);
inputGlobalDiscount.addEventListener("keyup", validateGlobalDiscount);
formInvoiceItems.addEventListener("submit", submitFormInvoiceitems);
selectRates.addEventListener("change", changeCurrency);
// ****** Events **********

function dateDocumentMaxToday() {
    let today = new Date().toJSON().split('T')[0];
    inputDateDocument.setAttribute("max", today);
}

function getRates() {
    fetch('https://api.exchangeratesapi.io/latest?base=MXN')
        .then(function (resp) {
            return resp.json();
        })
        .then(function (resp) {
            exchangesRates = resp;
        })
        .catch(function () {
            console.error('Error when obtaining exchange rates');
            exchangesRates = false;
        });
}

function addRates() {
    let option;
    option = document.createElement("option");
    option.text = 'MXN - 1';
    option.value = 1;
    option.dataset.currency = currencyGlobal;
    selectRates.appendChild(option);
    if (exchangesRates) {
        let rates = exchangesRates.rates;
        for (let index in rates) {
            if (index != currencyGlobal) {
                option = document.createElement("option");
                option.text = index + ' - ' + Math.round(rates[index] * 100) / 100;
                option.value = Math.round(rates[index] * 100) / 100;
                option.dataset.currency = index;
                selectRates.appendChild(option);
            }
        }
    }
}

function applyDiscount(e) {
    e.preventDefault();
}

function createInvoice(e) {
    e.preventDefault();
    let arrayInvoice = {
        dateDocument: document.querySelector('#date-document').value,
        clientKey: document.querySelector('#client-key').value,
        clientName: document.querySelector('#client-name').value,
        currency: currencyGlobal,
        currencyValue: currencyGlobalValue,
        discount: document.querySelector('#global-discount').value,
        pay: false,
        items: arrayInvoiceItems,
    }

    arrayInvoiceItems = [];
    addToLocalStorage('invoice-items', arrayInvoiceItems);

    arrayInvoices.push(arrayInvoice);
    addToLocalStorage('invoices', arrayInvoices);

    print_invoice(arrayInvoice);
    formInvoice.reset();
}

function changeCurrency(e) {
    e.preventDefault();
    arrayInvoiceItems = JSON.parse(localStorage.getItem('invoice-items'));
    if (arrayInvoiceItems === null) {
        arrayInvoiceItems = [];
    } else {
        arrayInvoiceItems.forEach(function (itemInvoice, index) {
            arrayInvoiceItems[index]['movPrice'] = arrayInvoiceItems[index]['movPrice'] / currencyGlobalValue;
        });
        currencyGlobal = selectRates.options[selectRates.selectedIndex].getAttribute("data-currency");
        currencyGlobalValue = selectRates.value;
        arrayInvoiceItems.forEach(function (itemInvoice, index) {
            arrayInvoiceItems[index]['movCurrency'] = currencyGlobal;
            arrayInvoiceItems[index]['movCurrencyVal'] = currencyGlobalValue;
            arrayInvoiceItems[index]['movPrice'] = arrayInvoiceItems[index]['movPrice'] * currencyGlobalValue;
        });
    }
    addToLocalStorage('invoice-items', arrayInvoiceItems);
}

function validateGlobalDiscount() {
    if (inputGlobalDiscount.value > 100) {
        inputGlobalDiscount.value = '100';
    } else if (inputGlobalDiscount.value < 0) {
        inputGlobalDiscount.value = '0';
    }
}

function submitFormInvoiceitems(e) {
    e.preventDefault();
    createInvoiceItems();
    addToLocalStorage('invoice-items', arrayInvoiceItems);
    formInvoiceItems.reset();
}

function createInvoiceItems() {
    let invoiceItem = {
        movCode: document.querySelector('#mov-code').value,
        movCurrency: currencyGlobal,
        movCurrencyVal: currencyGlobalValue,
        movIva: document.querySelector('#mov-iva').value,
        movPrice: document.querySelector('#mov-price').value,
        movQuantity: document.querySelector('#mov-quantity').value,
        movSubtotal: 0,
        movTaxes: 0,
        movDiscount: document.querySelector('#mov-discount').value,
        movDiscountTotal: 0,
    }
    arrayInvoiceItems.push(invoiceItem);
    return invoiceItem;
}

function removeInvoiceItems(e) {
    let indexArray;
    arrayInvoiceItems.forEach((elemento, index) => {
        if (elemento.e === e) {
            indexArray = index;
        }
    });
    arrayInvoiceItems.splice(indexArray, 1);
    addToLocalStorage('invoice-items', arrayInvoiceItems);
}

function insertIntoTableInvoiceItems() {

    let discountItem = 0;
    let subTotalItem = 0;
    let taxestItem = 0;
    let totalSubTotal = 0;
    let totalDiscount = 0;
    let totalPrice = 0;
    let totalQuantity = 0;
    let totalTaxes = 0;

    tableInvoiceItems.innerHTML = '';
    arrayInvoiceItems = JSON.parse(localStorage.getItem('invoice-items'));
    if (arrayInvoiceItems === null) {
        arrayInvoiceItems = [];
    } else {
        arrayInvoiceItems.forEach(function (itemInvoice, index) {
            discountItem = (itemInvoice.movQuantity * itemInvoice.movPrice) * (itemInvoice.movDiscount / 100);
            taxestItem = (itemInvoice.movQuantity * itemInvoice.movPrice) * (itemInvoice.movIva / 100);
            subTotalItem = (itemInvoice.movQuantity * itemInvoice.movPrice) + taxestItem - discountItem;

            arrayInvoiceItems[index]['movSubtotal'] = subTotalItem.toFixed(2);
            arrayInvoiceItems[index]['movTaxes'] = taxestItem.toFixed(2);
            arrayInvoiceItems[index]['movDiscountTotal'] = discountItem.toFixed(2);

            tableInvoiceItems.innerHTML += `
                <tr>
                    <td>${itemInvoice.movCode}</td>
                    <td>${itemInvoice.movQuantity}</td>
                    <td>$${itemInvoice.movPrice}</td>
                    <td>${itemInvoice.movCurrency}</td>
                    <td>${itemInvoice.movIva}%</td>
                    <td>$${itemInvoice.movTaxes}</td>
                    <td>${itemInvoice.movDiscount}%</td>
                    <td>$${itemInvoice.movDiscountTotal}</td>
                    <td>$${itemInvoice.movSubtotal}</td>
                    <td><a class="btn btn-md btn-danger" onclick="removeInvoiceItems(this)"><i class="fas fa-minus-circle"></i></a>
                    </td>
                </tr>
            `;

            totalDiscount += parseFloat(itemInvoice.movDiscountTotal);
            totalPrice += parseFloat(itemInvoice.movPrice * itemInvoice.movQuantity);
            totalQuantity += parseFloat(itemInvoice.movQuantity);
            totalSubTotal += parseFloat(itemInvoice.movSubtotal);
            totalTaxes += parseFloat(itemInvoice.movTaxes);
        });
        document.querySelector('.total-quantity').innerHTML = totalQuantity;
        document.querySelector('.total-price').innerHTML = '$' + totalPrice;
        document.querySelector('.total-taxes').innerHTML = '$' + totalTaxes;
        document.querySelector('.total-subtotal').innerHTML = '$' + totalSubTotal;
        document.querySelector('.total-discount').innerHTML = '$' + totalDiscount;
    }
}

function print_invoice(invoice) {
    let items = invoice.items;
    let totalDiscount = 0;
    let totalPrice = 0;
    let totalQuantity = 0;
    let totalSubTotal = 0;
    let totalTaxes = 0;
    
    console.log(items);

    document.querySelector('.print-cliente-name').innerHTML = 'Nombre del cliente: ' + invoice.clientName;
    document.querySelector('.print-date-document').innerHTML = 'Fecha del documento: ' + invoice.dateDocument;
    document.querySelector('.print-cliente-key').innerHTML = 'Clave del cliente: ' + invoice.clientKey;
    document.querySelector('.print-rates').innerHTML = 'Tipo de cambio: ' + invoice.currency;
    document.querySelector('.print-global-discount').innerHTML = 'Descuento global: ' + invoice.discount + '%';

    tableInvoiceItemsPrint.innerHTML = '';

    items.forEach(function (itemInvoice, index) {
        itemInvoice.movPrice = itemInvoice.movPrice.toFixed(2);

        totalDiscount += parseFloat(itemInvoice.movDiscountTotal);
        totalPrice += parseFloat(itemInvoice.movPrice * itemInvoice.movQuantity);
        totalQuantity += parseFloat(itemInvoice.movQuantity);
        totalSubTotal += parseFloat(itemInvoice.movSubtotal);
        totalTaxes += parseFloat(itemInvoice.movTaxes);
        
        tableInvoiceItemsPrint.innerHTML += `
            <tr>
                <td>${itemInvoice.movCode}</td>
                <td>${itemInvoice.movQuantity}</td>
                <td>$${itemInvoice.movPrice}</td>
                <td>${itemInvoice.movCurrency}</td>
                <td>${itemInvoice.movIva}%</td>
                <td>$${itemInvoice.movTaxes}</td>
                <td>${itemInvoice.movDiscount}%</td>
                <td>$${itemInvoice.movDiscountTotal}</td>
                <td>$${itemInvoice.movSubtotal}</td>
            </tr>
        `;
    });

    document.querySelector('.total-quantity-print').innerHTML = totalQuantity;
    document.querySelector('.total-price-print').innerHTML = '$' + totalPrice;
    document.querySelector('.total-taxes-print').innerHTML = '$' + totalTaxes.toFixed(2);
    document.querySelector('.total-subtotal-print').innerHTML = '$' + totalSubTotal.toFixed(2);
    document.querySelector('.total-discount-print').innerHTML = '$' + totalDiscount.toFixed(2);

    var win = window.open('', '', 'left=0,top=0,width=552,height=477,toolbar=0,scrollbars=0,status =0');
    var handler = function () {
        win.print();
        win.close();
    };
    if (win.addEventListener)
        win.addEventListener('load', handler, false);
    else if (win.attachEvent)
        win.attachEvent('onload', handler, false);

    var content = "<html>";
    content += document.getElementById("print-invoice").innerHTML;
    content += "</body>";
    content += "</html>";
    win.document.write(content);
    win.document.close();
}

// ****** local storage **********
function addToLocalStorage(key, values) {
    localStorage.setItem(key, JSON.stringify(values));
    insertIntoTableInvoiceItems();
}
// ****** local storage **********

document.addEventListener('DOMContentLoaded', function () {
    dateDocumentMaxToday();
    getRates();
    setTimeout(() => {
        addRates();
        insertIntoTableInvoiceItems();
    }, 1500);
});