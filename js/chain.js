// ****** Variables **********
const textString = document.querySelector('#text-string');
const getResultsBtn = document.querySelector('#get-results');

// ****** Variables **********

let textStringTmp = false;
let reverseStringVal = "";
let totalVowels = 0;
let stringEncrypt = false;

// ****** Events **********

textString.addEventListener("keyup", validateString);
getResultsBtn.addEventListener("click", getResultsString);

function validateString(e) {
    let letters = /^[0-9a-zA-Z]+$/;
    if (e.keyCode == 8) {
        return;
    }

    if (textString.value.match(letters)) {
        textStringTmp = textString.value;
    } else {
        textString.value = textStringTmp;
    }
}

function convertReverseString() {
    let reverseString = "";
    for (let x = textString.value.length - 1; x >= 0; x--) {
        reverseString += textString.value[x];
    }
    reverseStringVal = reverseString;
}

function countVowels() {
    totalVowels = textString.value.match(/[aeiou]/gi).length;
    console.log(totalVowels);
}

function printResults() {
    results.innerHTML = '';
    results.innerHTML += `
        <p>Cadena al revés ${reverseStringVal}</p>
        <p>Total de vocales ${totalVowels} </p>
    `;
}

function getResultsString(e) {
    convertReverseString();
    countVowels();
    printResults();
}



