// ****** Variables **********
const tableItems = document.querySelector('#table-water-items');
const btnGenerateItems = document.querySelector('#generate-items');
const results = document.querySelector('#results');

let arrayWater = [];
let total = 0;
let highestNumber = 0;
let lowestNumber = 0;
let avg = 0;
let highestDay = 0;
let lowestDay = 0;

// ****** Variables **********

// ****** Events **********

btnGenerateItems.addEventListener("click", generateTable);

function getRandomInt() {
    return Math.floor(Math.random() * (10000 - 0)) + 0;
}

function generateTable() {
    arrayWater = [];
    for (let index = 0; index < 30; index++) {
        arrayWater.push(getRandomInt());
    }
    tableItems.innerHTML = '';
    arrayWater.forEach(function (item, index) {
        tableItems.innerHTML += `
            <tr>
                <td>${index+1}</td>
                <td>${item} milímetros</td>
            </tr>
        `;
    });
    getResults();
}

function getResults() {
    total = arrayWater.reduce((a, b) => a + b, 0);
    highestNumber = Math.max.apply(null, arrayWater);
    lowestNumber = Math.min.apply(null, arrayWater);
    avg = total / arrayWater.length;
    arrayWater.forEach(function (item, index) {
        if (item === highestNumber) {
            highestDay = index;
        }
        if (item === lowestNumber) {
            lowestDay = index;
        }
    });
    printResults();
}

function printResults() {
    results.innerHTML = '';
    results.innerHTML += `
        <p>Dia de mayor lluvia, dia ${highestDay} con ${highestNumber} milímetros</p>
        <p>Dia de menor lluvia, dia ${lowestDay} con ${lowestNumber} milímetros</p>
        <p>Promedio ${avg.toFixed(2)} milímetros</p>
    `;
}

document.addEventListener('DOMContentLoaded', function () {
    generateTable();
});